package zerolog

import (
	"bytes"
	"context"
	"path"
	"strings"
	"time"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"github.com/rs/zerolog"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
)

var (
	Marshaller          = &jsonpb.Marshaler{}
	TimestampLog        = true
	ServiceField        = "service"
	ServiceLog          = true
	MethodField         = "method"
	MethodLog           = true
	DurationField       = "dur"
	DurationLog         = true
	IpField             = "ip"
	IpLog               = true
	MetadataField       = "md"
	MetadataLog         = true
	UserAgentField      = "ua"
	UserAgentLog        = true
	ReqField            = "req"
	ReqLog              = true
	RespField           = "resp"
	RespLog             = true
	MaxSize             = 2048000
	CodeField           = "code"
	MsgField            = "msg"
	DetailsField        = "details"
	UnaryMessageDefault = "unary"
	log                 = zerolog.Logger{}
)

// Logs gRPC service, method and duration of call.
//	{
//		ServiceField: ExampleService,
//		MethodField: ExampleMethod,
//		DurationField: 1.00,
//	}
func LogIncomingCall(logger *zerolog.Event, ctx context.Context, method string, t time.Time, req interface{}) {
	LogTimestamp(logger, t)
	LogService(logger, method)
	LogMethod(logger, method)
	LogDuration(logger, t)
	LogRequest(logger, req)
	LogIncomingMetadata(logger, ctx)
}

// Logs timestamp of call.
//	{
//		TimestampField: Timestamp,
//	}
func LogTimestamp(logger *zerolog.Event, t time.Time) {
	if TimestampLog {
		*logger = *logger.Time(zerolog.TimestampFieldName, t)
	}
}

// Logs gRPC service name.
//	{
//		ServiceField: gRPCServiceName,
//	}
func LogService(logger *zerolog.Event, method string) {
	if ServiceLog {
		*logger = *logger.Str(ServiceField, path.Dir(method)[1:])
	}
}

// Logs gRPC method call.
//	{
//		MethodField: gRPCMethodName,
//	}
func LogMethod(logger *zerolog.Event, method string) {
	if MethodLog {
		*logger = *logger.Str(MethodField, path.Base(method))
	}
}

// Logs duration in seconds of gRPC call.
//	{
//		DurationField: Timestamp,
//	}
func LogDuration(logger *zerolog.Event, t time.Time) {
	if DurationLog {
		*logger = *logger.Dur(DurationField, time.Since(t))
	}
}

// Logs IP address of gRPC client, if assigned.
//	{
//		IpField: 127.0.0.1
//	}
func LogIp(logger *zerolog.Event, ctx context.Context) {
	if IpLog {
		if p, ok := peer.FromContext(ctx); ok {
			*logger = *logger.Str(IpField, p.Addr.String())
		}
	}
}

// Logs JSON representation of gRPC Request, given Request is smaller than MaxSize (Default=2MB).
//	{
//		ReqField: {}
//	}
func LogRequest(e *zerolog.Event, req interface{}) {
	if ReqLog {
		if b := GetRawJson(req); b != nil {
			*e = *e.RawJSON(ReqField, b.Bytes())
		}
	}
}

// Logs JSON representation of gRPC Response, given Response is smaller than MaxSize (Default=2MB).
//	{
//		RespField: {}
//	}
func LogResponse(e *zerolog.Event, resp interface{}) {
	if RespLog {
		if b := GetRawJson(resp); b != nil {
			*e = *e.RawJSON(RespField, b.Bytes())
		}
	}
}

// Checks if an interface is a Protobuf message, smaller than MaxSize (Default=2MB) and converts to JSON bytes.
func GetRawJson(i interface{}) *bytes.Buffer {
	if pb, ok := i.(proto.Message); ok {
		b := &bytes.Buffer{}
		if err := Marshaller.Marshal(b, pb); err == nil && b.Len() < MaxSize {
			return b
		}
	}
	return nil
}

// Logs Metadata or UserAgent field of incoming gRPC Request, if assigned.
//	{
//		MetadataField: {
//			MetadataKey1: MetadataValue1,
//		}
//	}
//
//	{
//		UserAgentField: "Client-assigned User-Agent",
//	}
func LogIncomingMetadata(e *zerolog.Event, ctx context.Context) {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if MetadataLog {
			*e = *e.Dict(MetadataField, LogMetadata(&md))
			return
		} else if UserAgentLog {
			LogUserAgent(e, &md)
		}
	}
}

// Logs Metadata of gRPC Request
//	{
//		MetadataField: {
//			MetadataKey1: MetadataValue1,
//		}
//	}
func LogMetadata(md *metadata.MD) *zerolog.Event {
	dict := zerolog.Dict()
	for i := range *md {
		dict = dict.Str(i, strings.Join(md.Get(i), ","))
	}
	return dict
}

// Logs UserAgent of gRPC Client, if assigned.
//	{
//		UserAgentField: "Client-assigned User-Agent",
//	}
func LogUserAgent(logger *zerolog.Event, md *metadata.MD) {
	if ua := strings.Join(md.Get("user-agent"), ""); ua != "" {
		*logger = *logger.Str(UserAgentField, ua)
	}
}

// Logs Status error of gRPC Error Response.
//	{
//		Err: "An unexpected error occured",
//		CodeField: "Unknown",
//		MsgField: "Error message returned from the server",
//		DetailsField: [Errors],
//	}
func LogStatusError(logger *zerolog.Event, err error) {
	statusErr := status.Convert(err)
	*logger = *logger.Err(err).Str(CodeField, statusErr.Code().String()).Str(MsgField, statusErr.Message()).Interface(DetailsField, statusErr.Details())
}
