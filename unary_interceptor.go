package zerolog

import (
	"context"
	"time"

	"google.golang.org/grpc"
)

// Creates a gRPC Server Option that uses UnaryServerInterceptor() to log gRPC Requests.
func UnaryInterceptor() grpc.ServerOption {
	return grpc.UnaryInterceptor(UnaryServerInterceptor())
}

// Creates an Unary Server Interceptor that logs gRPC Requests using Zerolog.
//	{
//		ServiceField: "ExampleService",
//		MethodField: "ExampleMethod",
//		DurationField: 1.00
//
//		IpField: "127.0.0.1",
//
//		MetadataField: {},
//
//		UserAgentField: "ExampleClientUserAgent",
//		ReqField: {}, // JSON representation of Request Protobuf
//
//		Err: "An unexpected error occured",
//		CodeField: "Unknown",
//		MsgField: "Error message returned from the server",
//		DetailsField: [Errors],
//
//		RespField: {}, // JSON representation of Response Protobuf
//
//		ZerologMessageField: "UnaryMessageDefault",
//	}
func UnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		now := time.Now()
		resp, err := handler(ctx, req)
		if log.Error().Enabled() {
			if err != nil {
				logger := log.Error()
				LogIncomingCall(logger, ctx, info.FullMethod, now, req)
				LogStatusError(logger, err)
				logger.Msg(UnaryMessageDefault)
			} else if log.Info().Enabled() {
				logger := log.Info()
				LogIncomingCall(logger, ctx, info.FullMethod, now, req)
				LogResponse(logger, resp)
				logger.Msg(UnaryMessageDefault)
			}
		}
		return resp, err
	}
}
