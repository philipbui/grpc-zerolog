package zerolog

import (
	"fmt"

	"github.com/rs/zerolog"
	"google.golang.org/grpc/grpclog"
)

// Replaces gRPC Logger with new GrpcZeroLogger.
func GrpcLogSetNewZeroLogger() {
	grpclog.SetLoggerV2(NewGrpcZeroLogger(zerolog.Logger{}))
}

// Replaces gRPC Logger with GrpcZeroLogger.
func GrpcLogSetZeroLogger(logger GrpcZeroLogger) {
	grpclog.SetLoggerV2(logger)
}

type GrpcZeroLogger struct {
	log zerolog.Logger
}

func NewGrpcZeroLogger(logger zerolog.Logger) GrpcZeroLogger {
	return GrpcZeroLogger{log: logger}
}

func (l GrpcZeroLogger) Fatal(args ...interface{}) {
	l.log.Fatal().Msg(fmt.Sprint(args...))
}

func (l GrpcZeroLogger) Fatalf(format string, args ...interface{}) {
	l.log.Fatal().Msgf(format, args...)
}

func (l GrpcZeroLogger) Fatalln(args ...interface{}) {
	l.Fatal(args...)
}

func (l GrpcZeroLogger) Error(args ...interface{}) {
	l.log.Error().Msg(fmt.Sprint(args...))
}

func (l GrpcZeroLogger) Errorf(format string, args ...interface{}) {
	l.log.Error().Msgf(format, args...)
}

func (l GrpcZeroLogger) Errorln(args ...interface{}) {
	l.Error(args...)
}

func (l GrpcZeroLogger) Info(args ...interface{}) {
	l.log.Info().Msg(fmt.Sprint(args...))
}

func (l GrpcZeroLogger) Infof(format string, args ...interface{}) {
	l.log.Info().Msgf(format, args...)
}

func (l GrpcZeroLogger) Infoln(args ...interface{}) {
	l.Info(args...)
}

func (l GrpcZeroLogger) Warning(args ...interface{}) {
	l.log.Warn().Msg(fmt.Sprint(args...))
}

func (l GrpcZeroLogger) Warningf(format string, args ...interface{}) {
	l.log.Warn().Msgf(format, args...)
}

func (l GrpcZeroLogger) Warningln(args ...interface{}) {
	l.Warning(args...)
}

func (l GrpcZeroLogger) Print(args ...interface{}) {
	l.Info(args...)
}

func (l GrpcZeroLogger) Printf(format string, args ...interface{}) {
	l.Infof(format, args...)
}

func (l GrpcZeroLogger) Println(args ...interface{}) {
	l.Infoln(args...)
}

func (l GrpcZeroLogger) V(level int) bool {
	switch level {
	case 0:
		return zerolog.InfoLevel <= zerolog.GlobalLevel()
	case 1:
		return zerolog.WarnLevel <= zerolog.GlobalLevel()
	case 2:
		return zerolog.ErrorLevel <= zerolog.GlobalLevel()
	case 3:
		return zerolog.FatalLevel <= zerolog.GlobalLevel()
	default:
		panic("unhandled gRPC logger level")
	}
}
